import React, { Component } from "react";
import { withRouter } from "react-router";
import "./Result.css";
class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 0,
    };
  }

  readFromLocalStorage = (key, value) => {
    return localStorage.getItem(key);
  };

  componentDidMount() {
    let result =
      parseInt(this.readFromLocalStorage("counter1")) +
      parseInt(this.readFromLocalStorage("counter2"));

    this.setState({
      result: result ? result : 0
    })
  }

  render() {
    return (
      <div className="container">
        <h1>Result</h1>
        <div>
          Total: <span>{this.state.result}</span>
        </div>
        <div>
          <button className="redirect" onClick={() => this.props.history.push("/")}>
            Go Back
          </button>
        </div>
      </div>
    );
  }
}

export default withRouter(Result);
