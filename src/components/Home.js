import React from "react";
import { withRouter } from "react-router";
import "./Home.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.diff1 = 1;
    this.diff2 = 10;
    this.state = {
      name: "",
      isLoggedIn: false,
      count1: 0,
      count2: 0,
    };
  }

  increaseCount = (counterName) => {
    if (counterName === "counter1") {
      this.writeToLocalStorage("counter1", this.state.count1 + this.diff1);
      this.setState((state, props) => {
        return { count1: state.count1 + this.diff1 };
      });
    } else {
      this.writeToLocalStorage("counter2", this.state.count2 + this.diff2);
      this.setState((state, props) => {
        return { count2: state.count2 + this.diff2 };
      });
    }
  };

  decreaseCount = (counterName) => {
    if (counterName === "counter1") {
      this.writeToLocalStorage("counter1", this.state.count1 - this.diff1);
      this.setState((state, props) => {
        return { count1: state.count1 - this.diff1 };
      });
    } else {
      this.writeToLocalStorage("counter2", this.state.count2 - this.diff2);
      this.setState((state, props) => {
        return { count2: state.count2 - this.diff2 };
      });
    }
  };

  readFromLocalStorage = (key) => {
    return localStorage.getItem(key);
  };

  writeToLocalStorage = (key, value) => {
    localStorage.setItem(key, value);
  };

  handleSubmit = (event) => {
    if (this.state.name) {
      this.writeToLocalStorage("name", this.state.name);
      this.setState({
          isLoggedIn: true,
      })
    }
  };

  handleChange = (event) => {
    this.setState({
      name: event.target.value,
    });
  };

  componentDidMount() {
    let name = this.readFromLocalStorage("name");
    let isLoggedIn = name ? true : false;

    let count1 = parseInt(this.readFromLocalStorage("counter1"));
    let count2 = parseInt(this.readFromLocalStorage("counter2"));

    this.setState({
      name: name,
      isLoggedIn: isLoggedIn,
      count1: count1 ? count1 : 0,
      count2: count2 ? count2 : 0,
    });
  }

  render() {
    return (
      <div>
        <h2>Learn React By Doing!</h2>
        <div>
          {/* https://reactjs.org/docs/forms.html */}
          {this.state.isLoggedIn ? (
            <div className="header">{this.state.name}'s Counter</div>
          ) : (
            <form onSubmit={this.handleSubmit}>
              <label>
                Name:
                <input type="text" value={this.state.value} onChange={this.handleChange} />
              </label>
              <input type="submit" value="Log in" />
            </form>
          )}
        </div>
        <div>
          <h1 className="counter-display">{this.state.count1}</h1>
          <button onClick={() => this.increaseCount("counter1")}>+{this.diff1}</button>
          <button onClick={() => this.decreaseCount("counter1")}>-{this.diff1}</button>
        </div>
        <div>
          <h1 className="counter-display">{this.state.count2}</h1>
          <button onClick={() => this.increaseCount("counter2")}>+{this.diff2}</button>
          <button onClick={() => this.decreaseCount("counter2")}>-{this.diff2}</button>
        </div>
        <div>
          <button className="redirect" onClick={() => this.props.history.push("/results")}>View Results</button>
        </div>
      </div>
    );
  }
}

export default withRouter(App);
