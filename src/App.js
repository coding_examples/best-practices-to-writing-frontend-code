import { Route, Switch } from "react-router-dom";
import React, { Component } from "react";
import Home from "./components/Home"
import Result from "./components/Result";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/results">
            <Result />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    );
  }
}
